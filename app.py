from flask import Flask
from flask_bootstrap import Bootstrap
from flask import make_response
from flask import render_template
import hashlib

# start the flask app using flask micro framework
app = Flask(__name__)

# use bootstrap css template
Bootstrap(app)

# define default hash
last_hash = ""


# get the next otp
def next_otp(prev):
    global last_hash

    # determine if need to hash the old hash or a new key
    if not prev:
        new_hash = hashlib.sha256(str(get_key()).encode('utf-8')).hexdigest()

    # hash old hash
    else:
        new_hash = hashlib.sha256(str(prev).encode('utf-8')).hexdigest()

    return new_hash


#   initial key
def get_key():
    # Hard-code our key
    return '808670FF00FF08812'


#   creates the response the web server returns
@app.route('/')
def index():

    # renders the index.html template
    resp = make_response(render_template('index.html'))

    # WARNING! This is a terrible idea and you should never do this in production!
    # However, for a simple class project, sending our private key as a cookie is just fine.
    resp.set_cookie('key', get_key())
    return resp


#   checks to see if the hashes match
@app.route('/authenticate/<otp>')
def authenticate(otp):
    global last_hash

    # hash the old hash
    generated_hash = next_otp(last_hash)

    # check if hashes match in the next 100 hashes
    for i in range(0, 100):
        if otp == generated_hash[0:6]:
            # Commit our new hash
            last_hash = generated_hash
            return "Access Granted"

        generated_hash = next_otp(generated_hash)

    # if not found, hash is invalid
    return "Access Denied"


if __name__ == '__main__':
    app.run()
