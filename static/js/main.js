// Updates what iteration of the OTP we are on
function refresh_rounds() {

    // if otp_round is not false, returns the cookie, otherwise returns
    // "not started"
    const rounds = otp_round() ? otp_round() : "Not started";

    // displays the rounds in the html
    $(".rounds").html(rounds);
}

// Gets the number of rounds we are on in the otp
function otp_round(new_round = "") {
    const cookie_name = "round";

    // if a parameter was passed in, update the cookie with the new parameter
    if (new_round)
        return $.cookie(cookie_name, new_round);

    // else return old
    return $.cookie(cookie_name);
}

// handles otp hashes and cookies. Default parameter is empty string ""
// If no parameter is given, returns the last hash cookie
function otp_hash(new_hash = "") {
    const cookie_name = "last_hash";

    // if called with a parameter, sets a new cookie
    if (new_hash)
        return $.cookie(cookie_name, new_hash);

    return $.cookie(cookie_name);
}

// truncates the hash to 6 digits
function trunc_hash(hash) {
    // Truncate our hash to get the OTP
    return hash.substring(0, 6);
}

// uses the key to create a new otp or hash the last hash
function new_otp(key) {
    let current_round = 0;
    let new_hash = "";

    // check to see if we've done our first round
    if (otp_round() && otp_hash()) {
        // Just do another round
        current_round = otp_round();
        new_hash = sha256_digest(otp_hash());

    // if not, hash the last hash
    } else {
        new_hash = sha256_digest(key)
    }

    // increment the round and get the new cookie
    otp_round(parseInt(current_round) + 1);

    // hash it again
    otp_hash(new_hash);

    // refresh the number of rounds in the gui
    refresh_rounds();

    // return the truncated hash
    return trunc_hash(new_hash);
}

//  checks the otp entered in the form against the authenticate page
//  and returns the result
function check_otp(otp) {
    $.get("/authenticate/" + otp, function(data) {
        $(".server-result").html(data);
    });
}

// generate an analysis map for the OTPs
function gen_analysismap(key) {
    var last_hash = key;
    var otp_map = new Object();
    const jump_val = 100000;

    console.log("xval,collisions");
    for (var i = jump_val; i <= 1000000; i += jump_val) {
        if (i == jump_val)
            otp_map[i] = new Object();
        else
            otp_map[i] = $.extend(true, {}, otp_map[i - jump_val]);

        for (var j = i - jump_val; j < i; j++) {
            last_hash = sha256_digest(last_hash);
            var trimmed_hash = trunc_hash(last_hash);

            if (otp_map[i][trimmed_hash] == undefined)
                otp_map[i][trimmed_hash] = 0;
            else
                otp_map[i][trimmed_hash]++;
        }

        const col_name = "collisions";
        otp_map[i][col_name] = 0;
        for (key in otp_map[i]) {
            if (key != col_name)
                otp_map[i][col_name] += otp_map[i][key];
        }

        console.log(i + "," + otp_map[i][col_name]);
    }
}

// create the analysis graph
function create_graph() {
    // Set the dimensions of the canvas / graph
    var margin = {top: 30, right: 20, bottom: 30, left: 50},
        width = 600 - margin.left - margin.right,
        height = 270 - margin.top - margin.bottom;

    // Set the ranges
    var x = d3.scale.linear().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);

    // Define the axes
    var xAxis = d3.svg.axis().scale(x)
        .orient("bottom").ticks(5);

    var yAxis = d3.svg.axis().scale(y)
        .orient("left").ticks(5);

    // Define the line
    var valueline = d3.svg.line()
        .x(function (d) {
            return x(d.xval);
        })
        .y(function (d) {
            return y(d.collisions);
        });

    // Adds the svg canvas
    var svg = d3.select("#analyze")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    // Get the data
    d3.csv("/static/data.csv", function (error, data) {
        data.forEach(function (d) {
            d.collisions = +d.collisions;
        });

        // Scale the range of the data
        x.domain(d3.extent(data, function (d) {
            return d.xval;
        }));
        y.domain([0, d3.max(data, function (d) {
            return d.collisions;
        })]);

        // Add the valueline path.
        svg.append("path")
            .attr("class", "line")
            .attr("d", valueline(data));

        // Add the X Axis
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        // Add the Y Axis
        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);
    });
}

// Page initialization code
$(document).ready(function() {
    console.log("Starting up...");

    refresh_rounds();

    // enter placeholder values into page
    $(".otp-generate").click(function () {
        $(".otp-result").attr("placeholder", new_otp($.cookie("key")));
    });

    // function for button clicking
    $(".otp-check").click(function () {
        check_otp($(".otp-input").val());
    });

    // Create the cool graph
    create_graph();
});
